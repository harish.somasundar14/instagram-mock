import 'package:flutter/material.dart';

AppBar header(context,
    {bool isAppTitle = false,
    String stringTitle,
    backButtonVisibility = false}) {
  return AppBar(
    iconTheme: IconThemeData(
      color: Colors.white,
    ),
    automaticallyImplyLeading: backButtonVisibility ? false : true,
    title: Text(
      isAppTitle ? 'LinkedIn Mock' : stringTitle,
      style: TextStyle(
        color: Colors.white,
        fontFamily: isAppTitle ? 'Signatra' : '',
        fontSize: isAppTitle ? 45.0 : 22.0,
      ),
      overflow: TextOverflow.ellipsis,
    ),
    centerTitle: true,
    backgroundColor: Theme.of(context).accentColor,
  );
}
